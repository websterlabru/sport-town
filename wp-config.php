<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'u6633613_sport-town' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'u6633_sport-town' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'tMr987b%' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'CT~{IupKC=K+Nz}EUj.&vyWfOGAR08q1>K+X[.%FHGRA09FuN;!R.z?1 RtnM7{Q' );
define( 'SECURE_AUTH_KEY',  'vw9eD:M Pue%l>P#N+VhV,7^3[icqrheR*3fOcjU4M5ao+Y{o~o]xlHZ[cL}K6o@' );
define( 'LOGGED_IN_KEY',    'qb.^qs[N3Y7]S O!bZo=rDcsqQy$[ZVl,ET:INP,9}l2{g*d?+no0,sQ?0{*iqSb' );
define( 'NONCE_KEY',        'P1>]rEe>@V=@[RYMG#PfxU!NC 9_x1Q4h&U*n8[Vv*NVkk7<Rjs7bQdlh&HkiDHE' );
define( 'AUTH_SALT',        '#ds}xD6|Bagb,%VQo8ak,;AH@?yhCVRBs*v),G.i?+FX?]s)BPXr|1>#+/4OGV(S' );
define( 'SECURE_AUTH_SALT', 'HuQ9w-Hn<w5_uY`r@$AnDBqpr8eL7emr`/>za`tZ%I:<T-?cYp3apYu>d$8p?*Y ' );
define( 'LOGGED_IN_SALT',   ']9[%;E,7g0I$l<z|p+[Gx<Wnh Ryg:qzRbtyTi@O <<{/z$F~l]A!$agjny5P{:K' );
define( 'NONCE_SALT',       'P[8??Y9G*|L${bv}$8u7*H.m@(05kc2>;sc(gAzKu[*d8fU+m/iR!Vll&X.iS:m/' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
