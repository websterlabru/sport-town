<?php

/*Поддержка темы*/
add_theme_support('post-thumbnails'); // поддержка миниатюр
/*End*/

/*Регистрируем меню*/
add_action('after_setup_theme', function(){
	register_nav_menus( array(
		'header_menu' => 'Меню в шапке',
		'footer_menu' => 'Меню в подвале'
	) );
});
/*End*/

/*Регистрируем виджеты*/
if (function_exists('register_sidebar')){
	register_sidebar([
		'name' => 'Footer',
		'id' => 'footer-sidebar',
		'description' => 'Эти виджеты будут показаны в нижней части сайта',
		'before_widget' => '<div class="footer-widget col">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	]);
}
/*End*/

/*Добавляем класс пунктам меню li*/
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    $classes[] = 'nav-item';
    return $classes;
}
/*End*/

/*Добавляем класс ссылкам в меню*/
add_filter('nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4);
function filter_nav_menu_link_attributes($atts, $item, $args, $depth){
  $atts['class'] .= 'nav-link';
  return $atts;
}
/*End*/

/* Post Type: Команда */
function cptui_register_my_cpts_teams() {

	$labels = [
		"name" => __( "Команда", "custom-post-type-ui" ),
		"singular_name" => __( "Специалист", "custom-post-type-ui" ),
		"all_items" => __( "Все специалисты", "custom-post-type-ui" ),
		"add_new" => __( "Новый специалист", "custom-post-type-ui" ),
		"not_found" => __( "Специалистов не найдено", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Специалистов не найдено", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Команда", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "teams", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-groups",
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "teams", $args );
}

add_action( 'init', 'cptui_register_my_cpts_teams' );
/* End */

?>
