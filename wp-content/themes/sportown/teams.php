<?
/*
    Template Name: Команда
    Template Post Type: page
*/

/**
 * Команда (teams.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<? get_header(); ?>

<?
  $query = new WP_Query([
    'post_type' => 'teams',
    'posts_per_page' => '10',
    // 'meta_query' => [
    //   'relation' => 'OR',
    //   [
    //     'key' => 'programs',
    //     'value' => 'p2',
    //     'compare' => 'LIKE'
    //   ],
    //   [
    //     'key' => 'programs',
    //     'value' => 'p3',
    //     'compare' => 'LIKE'
    //   ]
    // ]
  ]);
?>


<?
  $field_key = "field_5f0320e60c0a0";
  $field = get_field_object($field_key);
?>
<div class="container">
  <div class="row">
    <div class="col mb-3">
      <? if($field): ?>
      <div class="checkselect">
      	<fieldset>
          <? foreach($field['choices'] as $k => $v): ?>
      		<label><input name="brands[]" value="<?=$k;?>" type="checkbox"> <?=$v;?></label>
        <? endforeach; ?>
      	</fieldset>
      </div>
    <? endif; ?>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <? if($query->have_posts()): while($query->have_posts()): $query->the_post(); ?>
      <div class="col-xl-6">
        <div class="row teams no-gutters overflow-hidden flex-md-row mb-4 position-relative">
          <div class="col-auto">
            <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium'); ?>
            <img src="<?=$thumbnail_attributes[0];?>" width="240" height="320">
          </div>
          <div class="col p-4 d-flex flex-column position-static">
            <h1 class="mb-0"><? the_title(); ?></h1>
            <p class="teams-text mb-auto"><? the_excerpt(); ?></p>
            <p class="teams-stage mb-auto">Тренерский стаж - <? the_field('coaching_staff', get_the_ID()); ?></p>
            <a href="<? the_permalink(); ?>" class="teams-more stretched-link">Подробнее</a>
          </div>
        </div>
      </div>
      <? //the_content(); ?>
      <? wp_reset_query(); ?>
    <? endwhile; else: ?>
      <pre>Записей не найдено</pre>
    <? endif; ?>
  </div>
</div>

<? get_footer(); ?>
