<footer class="social-footer">
  <div class="container">
    <div class="row justify-content-center">
      <div class="social col-lg-2 col-md-4">
        <a href="https://vk.com/sportown" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/icons/vk.svg"/></a>
      </div>
      <div class="social col-lg-2 col-md-4">
        <a href="https://www.facebook.com/sportown.fitness" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/icons/fb.svg"/></a>
      </div>
      <div class="social col-lg-2 col-md-4">
        <a href="https://www.instagram.com/sportown_fit/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/icons/instagram.svg"/></a>
      </div>
    </div>
  </div>
</footer>

<footer class="footer">
  <div class="container">
    <div class="row">
      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-sidebar')); ?>
    </div>
    <div class="row mt-5">
      <div class="col">
        <h4>Скачать приложение</h4>
        <a href="https://apps.apple.com/app/id1273747943" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/icons/appstore.svg" class="mr-2"/></a>
        <a href="https://play.google.com/store/apps/details?id=com.itrack.sportown592186" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/icons/googleplay.svg"/></a>
      </div>
      <div class="col phone text-sm-right">+7 (495) 125-42-85</div>
    </div>
    <div class="row site-copy">
      <div class="col">© Sportown <?=date('Y');?> <a href="#">Политика конфиденциальности</a></div>
    </div>
  </div>
</footer>

<!-- Плагин checkselect -->
<script src="<?php bloginfo('template_url'); ?>/js/checkselect.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/checkselect.css">
<!-- End -->

<?php wp_footer(); ?>
</body>
</html>
