<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name') ?> <? wp_title();?></title>

  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
  <!-- jQuery End -->

	<!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<!-- Bootstrap End-->

	<!-- Stylesheet for this theme -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<!-- Stylesheet for this theme End -->

	<script>
		$(document).ready(function(){

		});
	</script>
  <?php wp_head(); ?>
</head>
<body>

<nav class="navbar navbar-expand-md">
  <div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
	    <i class="fas fa-bars"></i>
	  </button>
    <a href="/" class="navbar-brand"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" alt=""></a>
		<div class="header-menu">
			<div class="header-phone">
				<div class="phone mr-4">+7 (495) 125-42-85</div>
				<div class="callback">Обратный звонок</div>
			</div>
	    <div class="collapse navbar-collapse" id="navbarMain">
	      <?php wp_nav_menu(array(
	                        'theme_location' => 'header_menu',
	                        'menu_class'     => 'navbar-nav ml-auto',
	                        'container'      => false
	                      )); ?>
	    </div>
		</div>
  </div>
</nav>
