<?php get_header(); ?>

<div class="container">
  <div class="row">
    <div class="col-lg-4 pl-0">
      <div class="block-h" style="background: #fcc;">Block 1</div>
    </div>
    <div class="col-lg-8 pr-0" style="background: #ffc;">
      <div style="background: #ffc;">
        <h1>Почувствуй фитнес по новому!</h1>
        <div class="description">Новый год — время обновлений и самых выгодных предложений в Sportown.  Только 4 дня фитнес по небывалым ценам со скидками до 50% и мегаподарки к вашей карте!</div>
        <a href="#" class="home-btn btn">Кнопка</a>
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-lg-8 pl-0">
      <div class="block-h" style="background: #fcc;">Block 1</div>
    </div>
    <div class="col-lg-4 pr-0" style="background: #ffc;">
      <div style="background: #ffc;">Block 2</div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-lg-4 pl-0">
      <div class="block-h" style="background: #fcc;">Block 1</div>
    </div>
    <div class="col-lg-8 pr-0" style="background: #ffc;">
      <div style="background: #ffc;">Block 2</div>
    </div>
  </div>
</div>

<div class="container">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article>
      <? the_content(); ?>
    </article>
  <? endwhile; else: ?>
    <h1>Страница не найдена</h1>
  <? endif; ?>
</div>

<?php get_footer(); ?>
