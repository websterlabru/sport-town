<? get_header(); ?>

<div class="container">
  <div class="row">
    <div class="col-12 my-5 text-center">
      <img src="<?php bloginfo('template_url'); ?>/img/404.png" class="img-fluid" alt="Страница не найдена">
    </div>
    <div class="col-12 text-center mb-5">
      <a href="/" class="btn btn-primary">На главную</a>
    </div>
  </div>
</div>

<? get_footer(); ?>
